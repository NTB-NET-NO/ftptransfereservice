Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data.OleDb
Imports System.Text
Imports System.IO
' Imports System.Web.Mail
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Net.Sockets

Module ModulePublic

    Public Const TEXT_INIT As String = "NTB_FtpRoute Init finished"
    Public Const TEXT_STARTED As String = "NTB_FtpRoute Service Started"
    Public Const TEXT_STOPPED As String = "NTB_FtpRoute Service Stopped"
    Public Const TEXT_PAUSE As String = "NTB_FtpRoute Service Paused"
    Public Const TEXT_LINE As String = "----------------------------------------------------------------------------------"
    Public Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Public myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    Public databaseOfflinePath As String = ConfigurationManager.AppSettings("databaseOfflinePath")
    Public deleteTempFiles As Boolean = UCase(ConfigurationManager.AppSettings("deleteTempFiles")) <> "NO"
    Public errFilePath As String = ConfigurationManager.AppSettings("errFilePath")
    Public tempPath As String = ConfigurationManager.AppSettings("tempPath")
    Public sqlConnectionString As String = ConfigurationManager.AppSettings("SqlConnectionString")

    Public smtpServer As String = ConfigurationManager.AppSettings("smtp")
    Public errorMailAddress As String = ConfigurationManager.AppSettings("errorMailAddress")

    Public sleepSec As Integer = ConfigurationManager.AppSettings("sleepSec")
    Public intPollingInterval As Integer = ConfigurationManager.AppSettings("pollingInterval")

    Public ftpTimeout As Integer = ConfigurationManager.AppSettings("FTPTimeout")

    Public intErrRetry As Integer = ConfigurationManager.AppSettings("errRetry")
    Public intErrorRetryInterval As Integer = ConfigurationManager.AppSettings("errorRetryInterval")

    Public cn As New OleDbConnection(sqlConnectionString)
    Public logFilePath As String = ""

    Public Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As OleDbDataAdapter, ByVal command As OleDbCommand, Optional ByVal strGroupListID As String = "")
        Dim strFile As String = databaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"

        WriteLog(logFilePath, "stringFile: " & strFile)
        command.CommandText = strCommand
        If cn.State = ConnectionState.Open Then
            Try
                WriteLog(logFilePath, "Using database-connection to write XMLfile " & strFile)
                dataAdapter.Fill(dsDataSet)
                dsDataSet.WriteXml(strFile, XmlWriteMode.WriteSchema)
            Catch
                WriteLog(logFilePath, "Using database-connection to read XMLfile " & strFile)
                dsDataSet.ReadXml(strFile, XmlWriteMode.WriteSchema)
            End Try
        Else
            WriteLog(logFilePath, "No connection to database. Using database-connection to read XMLfile " & strFile)
            dsDataSet.ReadXml(strFile, XmlWriteMode.WriteSchema)
        End If
    End Sub

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

    End Sub

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

    End Sub

    Public Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim strLine As String

        strLine = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False)
        Dim w As New StreamWriter(strFileName, append, myEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, myEncoding)    ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

    Public Function SendErrMail(ByVal strMessage As String) As Boolean

        Dim message As New MailMessage
        Dim smtpMail As New SmtpClient(smtpServer)

        Dim sendFrom As New MailAddress(errorMailAddress)
        Dim sendTo As New MailAddress(errorMailAddress)

        Dim mailMessage As New MailMessage()
        mailMessage.Body = strMessage
        mailMessage.IsBodyHtml = False
        mailMessage.Subject = "Error in NTB_FtpRouteService"
        mailMessage.BodyEncoding = myEncoding

        Try
            smtpMail.Send(mailMessage)

        Catch
            Return False
        End Try

        Return True
    End Function

End Module
