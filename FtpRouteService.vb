Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Data.OleDb
Imports System.ServiceProcess

#If Debug Then
Public Module FtpRouteService
#Else
Public Class FtpRouteService : Inherits ServiceBase
#End If

    'Dim Debug
    '-- Lokale variabler --

    Private collFtpRoute As Collection '= New Collection()

    Private dsSystem As DataSet
    Private dsCustomerFtp As DataSet

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

#If Debug Then
	' For debug: Run as form
	Public Sub Main()

		'FtpRoute.TestFtp()

        InitFtpRoute()
		'Start()
		'StopWait()
		Dim strTest As String
		Do
			Console.WriteLine("MENY: ")
			Console.WriteLine("P = Pause")
			Console.WriteLine("S = Start")
			Console.WriteLine("R = Reinit")
			Console.WriteLine("D = Dispose")
			Console.WriteLine("Q = Quit: ")
			Console.Write("Trykk menyvalg: ")
			strTest = Console.ReadLine
			Select Case UCase(strTest)
				Case "Q"
					Exit Do
				Case "P"
					StopWait()
				Case "S"
					Start()
				Case "R"
					'Reinit()
					DisposeObjects()
					InitFtpRoute()
					Start()
				Case "D"
					DisposeObjects()
			End Select
		Loop
		StopWait()

	End Sub
#Else

    ' For Release: run as service
    Public Shared Sub Main()
        ServiceBase.Run(New FtpRouteService())
    End Sub

    Public Sub New()
        MyBase.New()
        CanPauseAndContinue = False
        CanStop = True
        ServiceName = "NTB_FtpRoute"
    End Sub

    Protected Overrides Sub OnStop()
        DisposeObjects()
        EventLog.WriteEntry(TEXT_STOPPED)
        WriteLogNoDate(logFilePath, TEXT_LINE)
        WriteLog(logFilePath, TEXT_STOPPED)
        'End
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            InitFtpRoute()
            Start()
            EventLog.WriteEntry(TEXT_STARTED)
        Catch e As Exception
            WriteErr(errFilePath, "Feil i initiering av programmet", e)
            End
        End Try
    End Sub
#End If

    Private Sub InitFtpRoute()
        Dim intCount As Integer = 0
        Dim dtStartTime As Date
        Dim rowCustJobs As DataRow
        Dim objFtpRoute As FtpRoute

        Dim picJobs As Hashtable = New Hashtable(5)

        dtStartTime = Now

#If Not Debug Then
        Try
#End If

            Directory.CreateDirectory(tempPath)
            Directory.CreateDirectory(databaseOfflinePath)
            Directory.CreateDirectory(errFilePath)

            OpenDatabase()
            FillDataSets()
            CloseDatabase()

            ' We are fetching the path to the log for FTP, and yes, the typo is pointing to correct table in the database
            logFilePath = dsSystem.Tables(0).Rows(0)("logFptFilePath") & ""
            Directory.CreateDirectory(logFilePath)

            collFtpRoute = New Collection

            For Each rowCustJobs In dsCustomerFtp.Tables(0).Rows
                objFtpRoute = New FtpRoute(rowCustJobs)
                collFtpRoute.Add(objFtpRoute)
                WriteLog(logFilePath, "Initiert Ftp-objekt for: " & rowCustJobs("OutPath"))

                'Create bilde-job
                If rowCustJobs("PushPics") And Directory.Exists(rowCustJobs("defOutPath") & "\bilder") And Not picJobs.ContainsKey(rowCustJobs("defOutPath")) Then
                    rowCustJobs("OutPath") = rowCustJobs("defOutPath") & "\bilder"
                    rowCustJobs("SubDir") = "bilder"

                    objFtpRoute = New FtpRoute(rowCustJobs)
                    collFtpRoute.Add(objFtpRoute)
                    picJobs.Add(rowCustJobs("defOutPath"), True)
                    WriteLog(logFilePath, "Initiert Ftp-objekt for: " & rowCustJobs("OutPath"))
                End If

            Next
            WriteLog(logFilePath, TEXT_INIT)

#If Not Debug Then
        Catch e As Exception
            WriteErr(errFilePath, "Feil i initiering av XmlRoute", e)
            End
        End Try
#End If

    End Sub

    Private Sub StopWait()
        Dim objFtpRoute As FtpRoute
        For Each objFtpRoute In collFtpRoute
            objFtpRoute.Pause()
        Next

        For Each objFtpRoute In collFtpRoute
            objFtpRoute.WaitWhileBusy(2)
        Next
        WriteLogNoDate(logFilePath, TEXT_LINE)
        WriteLog(logFilePath, TEXT_PAUSE)
    End Sub

    Private Sub Start()
        Dim objFtpRoute As FtpRoute
        For Each objFtpRoute In collFtpRoute
            objFtpRoute.Start()
        Next
        WriteLog(logFilePath, TEXT_STARTED)
        WriteLogNoDate(logFilePath, TEXT_LINE)
    End Sub

    Private Function OpenDatabase() As Boolean
        Try
            cn.Open()
        Catch e As Exception
            WriteErr(errFilePath, "Feil i OpenDatabase: cn.Open", e)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillDataSets()
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()

        command.Connection = cn
        dataAdapter.SelectCommand = command

        dsSystem = New DataSet()
        dsCustomerFtp = New DataSet()

        FillOneDataSet(dsSystem, "dsSystem", "SELECT * FROM System", dataAdapter, command)
        FillOneDataSet(dsCustomerFtp, "dsCustomerFtp", "SELECT * FROM GetCustomerFtp", dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing
    End Sub

    Private Sub CloseDatabase()
        cn.Close()
    End Sub


    Private Sub DisposeObjects()
#If Not Debug Then
        Try
#End If
            Dim objFtpRoute As FtpRoute

            StopWait()

            For Each objFtpRoute In collFtpRoute
                'objXmlRoute.Dispose()
                objFtpRoute = Nothing
            Next

            collFtpRoute = Nothing
            collFtpRoute = New Collection()

            dsCustomerFtp = Nothing

            cn = Nothing
#If Not Debug Then
        Catch e As Exception
            WriteErr(errFilePath, "Feil i DisposeObjects", e)
            End
        End Try
#End If
    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Dim xmlDoc As XmlDocument = New XmlDocument()

    '    Try
    '        xmlDoc.LoadXml("<xml>" & Me.TextBox1.Text & "</xml>")
    '    Catch err As XmlException
    '        MsgBox(err.Message & ": " & err.LinePosition)
    '    End Try

    '    Me.TextBox2.Text = XchgReplace.ConvXmlEntities(Me.TextBox1.Text)

    'End Sub
#If Debug Then

End Module
#Else
End Class
#End If
