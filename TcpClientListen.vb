'Imports System.Net.Sockets
'Imports System.Text

'Public Class TcpClientListen

'    Public Function TcpSend()

'        Dim tcpClient As New tcpClient()
'        Try
'            tcpClient.Connect("www.contoso.com", 11000)
'            Dim networkStream As networkStream = tcpClient.GetStream()

'            If networkStream.CanWrite And networkStream.CanRead Then

'                ' Does a simple write.
'                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes("Is anybody there")
'                networkStream.Write(sendBytes, 0, sendBytes.Length)

'                ' Reads the NetworkStream into a byte buffer.
'                Dim bytes(tcpClient.ReceiveBufferSize) As Byte
'                networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))

'                ' Returns the data received from the host to the console.
'                Dim returndata As String = Encoding.ASCII.GetString(bytes)
'                Console.WriteLine(("This is what the host returned to you: " + returndata))

'            Else
'                If Not networkStream.CanRead Then
'                    Console.WriteLine("You can not write data to this stream")
'                    tcpClient.Close()
'                Else
'                    If Not networkStream.CanWrite Then
'                        Console.WriteLine("You can not read data from this stream")
'                        tcpClient.Close()
'                    End If
'                End If
'            End If
'        Catch e As Exception
'            Console.WriteLine(e.ToString())
'        End Try
'    End Function

'    Public Function TcpListen()
'        Const portNumber As Integer = 13
'        Dim tcpListener As New TcpListener(portNumber)

'        TcpListener.Start()

'        Console.WriteLine("Waiting for a connection....")

'        Try

'            'Accept the pending client connection and return a TcpClient initialized for communication. 
'            Dim tcpClient As TcpClient = tcpListener.AcceptTcpClient()
'            Console.WriteLine("Connection accepted.")

'            Dim networkStream As NetworkStream = tcpClient.GetStream()

'            Dim responseString As String = "You have successfully connected to me."

'            Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(responseString)
'            networkStream.Write(sendBytes, 0, sendBytes.Length)

'            Console.WriteLine(("Message Sent /> : " + responseString))

'            'Any communication with the remote client using the TcpClient can go here.
'            '
'            '//////
'            'Close TcpListener and TcpClient.
'            tcpClient.Close()
'            tcpListener.Stop()

'        Catch e As Exception
'            Console.WriteLine(e.ToString())
'        End Try
'    End Function


'End Class
