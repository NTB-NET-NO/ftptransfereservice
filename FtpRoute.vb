Imports System.Net
Imports System.IO
Imports System.Configuration

Public Class FtpRoute

    Private WithEvents Timer As System.Timers.Timer = New System.Timers.Timer()
    Private WithEvents FileWatcher As FileSystemWatcher = New FileSystemWatcher()

    Private bBusy As System.Threading.Mutex = New Threading.Mutex(False)
    Private bErrSendt As Boolean = False
    Private bFailed As Boolean = False
    Private bPaused As Boolean = False

    'Private bEnableRaisingEvents As Boolean
    'Private bTimerEnabled As Boolean
    'Private bEnableRaisingEvents2 As Boolean
    'Private bTimerEnabled2 As Boolean

    Private logCustFilePath As String

    Private jobID As String
    Private kunde As String
    Private ftpHost As String
    Private passive As Boolean = True
    Private username As String
    Private password As String
    Private outPath As String
    Private subDir As String
    Private delay As Integer

    Private strFtpProgramArgs As String
    Private strFtpParamFile As String
    Private strFtpLogFile As String
    Private strFtpErrorFile As String

    Private intFailCount As Integer = 0

    Public Sub New(ByVal rowCustFtp As DataRow)

        Try
            jobID = rowCustFtp("customerID") & "-" & rowCustFtp("GroupJobID")
            kunde = rowCustFtp("customerName")
            ftpHost = rowCustFtp("ftpHost")
            passive = rowCustFtp("passive")
            username = rowCustFtp("username")
            password = rowCustFtp("password")
            outPath = rowCustFtp("outPath")
            delay = rowCustFtp("PushDelay")
            subDir = rowCustFtp("subDir")
        Catch
        End Try

        If subDir = "" Then subDir = "."

        Dim kunde_file As String
        kunde_file = System.Text.RegularExpressions.Regex.Replace(kunde, "[ ������\\/$:]", "_")
        kunde_file &= "-" & rowCustFtp("GroupJobID")

        strFtpParamFile = tempPath & "\Config\FtpJob_" & kunde_file & ".cfg"
        strFtpLogFile = tempPath & "\FtpJobLog_" & kunde_file & ".log"
        strFtpErrorFile = tempPath & "\FtpJobError_" & kunde_file & ".log"
        logCustFilePath = logFilePath & "\CustJob_" & kunde_file

        If subDir.ToUpper = "BILDER" Then
            logCustFilePath &= "_Pics"
            strFtpParamFile = strFtpParamFile.Replace(".cfg", "_pics.cfg")
            strFtpLogFile = strFtpLogFile.Replace(".log", "_pics.log")
            strFtpErrorFile = strFtpErrorFile.Replace(".log", "_pics.log")
        End If

        Dim strFtpParam As String = ""
        strFtpParam = "host " & ftpHost & vbCrLf
        strFtpParam &= "user " & username & vbCrLf
        strFtpParam &= "pass " & password & vbCrLf
        WriteFile(strFtpParamFile, strFtpParam)

        ' Added -S .tmp as parameter - this will save the file as .tmp before it is uploaded
        'strFtpProgramArgs = "-S .tmp -t " & ftpTimeout & " -V -m -DD -f """ & strFtpParamFile & """ -d """ & strFtpLogFile & """ -e """ & strFtpErrorFile & """ """ & subDir & """ "
        strFtpProgramArgs = "-t " & ftpTimeout & " -V -m -DD -f """ & strFtpParamFile & """ -d """ & strFtpLogFile & """ -e """ & strFtpErrorFile & """ """ & subDir & """ "

        If Not passive Then
            strFtpProgramArgs = "-E " & strFtpProgramArgs
        End If

        Directory.CreateDirectory(logCustFilePath)

        'Set up timers and watchers
        FileWatcher.Path = outPath
        Timer.Interval = intPollingInterval * 1000

    End Sub

    Public Sub Start()
        'Set off timers
        FileWatcher.EnableRaisingEvents = False

        Timer.Interval = 1000
        Timer.Enabled = True
    End Sub

    Public Sub Pause()
        bPaused = True
    End Sub

    Private Sub Timer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer.Elapsed
        'For error retries and leftover files.
        bBusy.WaitOne()
        Timer.Enabled = False
        FileWatcher.EnableRaisingEvents = False

        DoAllInputFiles()

        If bFailed Then
            If intFailCount <= intErrRetry And intFailCount > 1 Then
                Timer.Interval += intErrorRetryInterval * 1000
            ElseIf intFailCount > intErrRetry Then
                Timer.Interval = 15 * 60 * 1000
            End If
        Else
            Timer.Interval = intPollingInterval * 1000
        End If

        FileWatcher.EnableRaisingEvents = Not bPaused And Not bFailed
        Timer.Enabled = Not bPaused
        bBusy.ReleaseMutex()
    End Sub

    Private Sub FileWatcher_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileWatcher.Created
        bBusy.WaitOne()
        Timer.Enabled = False
        FileWatcher.EnableRaisingEvents = False

        DoAllInputFiles()

        If bFailed Then
            If intFailCount <= intErrRetry And intFailCount > 1 Then
                Timer.Interval += intErrorRetryInterval * 1000
            ElseIf intFailCount > intErrRetry Then
                Timer.Interval = 15 * 60 * 1000
            End If
        Else
            Timer.Interval = intPollingInterval * 1000
        End If

        FileWatcher.EnableRaisingEvents = Not bPaused And Not bFailed
        Timer.Enabled = Not bPaused
        bBusy.ReleaseMutex()
    End Sub

    Private Function DoOneInputFile(ByVal strFileName As String) As Boolean
        Dim bTransfereOk As Boolean = False
        Dim strLogResult As String = ""

        Try
            bTransfereOk = FtpTransfer(strFileName, strLogResult)
        Catch e As Exception
            WriteErr(logCustFilePath, "Transfer feilet: " & strFileName, e)
            bFailed = True
            Exit Function
        End Try

        If bTransfereOk Then
            bErrSendt = False
            bFailed = False
            intFailCount = 0

            WriteLog(logCustFilePath, "Slettet: " & strFileName)

            'Try
            '    File.Delete(strFileName)
            '    WriteLog(logCustFilePath, "Slettet: " & strFileName)
            'Catch e As Exception
            '    WriteErr(logCustFilePath, "Sletting Feilet: " & strFileName, e)
            'End Try
        Else
            bFailed = True
            intFailCount += 1

            If Not bErrSendt Then
                Dim msg As String
                msg = "Overf�ring til " & kunde & " feilet." & vbCrLf
                msg &= "Filnavn: " & strFileName & vbCrLf & vbCrLf
                msg &= strLogResult

                bErrSendt = SendErrMail(msg)
            End If

            WriteErr(logCustFilePath, "Feilet for " & intFailCount & ". gang." & vbCrLf & strLogResult & vbCrLf & vbCrLf)
        End If

        Return bTransfereOk
    End Function

    Private Sub DoAllInputFiles() 'ByRef rowGroupList As DataRow, ByRef intCount As Integer)
        Dim arrFileList() As String
        Dim strInputFile As String
        Static intCount As Long

        'Read all files in folder
#If Not Debug Then
        Try
#End If
        arrFileList = Directory.GetFiles(outPath)
#If Not Debug Then
        Catch e As Exception
            WriteErr(logFilePath, "Feil input directory: " & outPath, e)
            Exit Sub
        End Try
#End If
        'Wait some seconds for last inputfile file to be written by external program, before reading
        Threading.Thread.Sleep(sleepSec * 1000)
        For Each strInputFile In arrFileList

            'If delay = 0 Or Now.Subtract(File.GetLastWriteTime(strInputFile)).TotalMinutes > delay Then
            If Not DoOneInputFile(strInputFile) Then Exit Sub
            'End If

            intCount += 1
            'Application.DoEvents()
        Next
    End Sub

    Private Function FtpTransfer(ByVal strFileName As String, ByRef strLogResult As String) As Boolean

        Dim retval As Integer

        Dim st As ProcessStartInfo = New ProcessStartInfo()
        Dim p As Process

        st.FileName = "ncftpput.exe"

        If Convert.ToBoolean(ConfigurationManager.AppSettings("debug")) = True Then
            ' Line below added by Trond for debugging 2011-11-21
            WriteLog(logCustFilePath, "FTP Program Arguments: " & strFtpProgramArgs)
        End If

        st.Arguments = strFtpProgramArgs & """" & strFileName & """"
        st.UseShellExecute = False
        st.CreateNoWindow = True
        st.WindowStyle = ProcessWindowStyle.Hidden

        p = System.Diagnostics.Process.Start(st)

        p.WaitForExit()
        retval = p.ExitCode
        p.Close()

        If retval > 0 Then
            WriteLog(logCustFilePath, "FEILET! ( " & retval & " ): " & strFileName & " til: " & ftpHost)
            strLogResult = ReadFile(strFtpLogFile)
            If Convert.ToBoolean(ConfigurationManager.AppSettings("debug")) = False Then
                File.Delete(strFtpLogFile)
            End If
            Return False
        Else
            WriteLog(logCustFilePath, "Fila er overf�rt: " & strFileName & " til: " & ftpHost)
            strLogResult = ReadFile(strFtpLogFile)
            If Convert.ToBoolean(ConfigurationManager.AppSettings("debug")) = False Then
                File.Delete(strFtpLogFile)
            End If
            Return True
        End If

    End Function

    Public Function WaitWhileBusy(ByVal SleepRetrySeconds As Integer, Optional ByVal TimeOutSeconds As Integer = 15) As Boolean
        Dim i As Integer
        For i = 0 To TimeOutSeconds / SleepRetrySeconds
            If bBusy.WaitOne(SleepRetrySeconds * 1000, False) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Sub New()

    End Sub
End Class
