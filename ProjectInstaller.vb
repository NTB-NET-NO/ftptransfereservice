Imports System
Imports System.Collections
Imports System.Configuration.Install
Imports System.ServiceProcess
Imports System.ComponentModel

<RunInstaller(True)> Public Class ProjectInstaller : Inherits installer

    Private serviceInstaller As ServiceInstaller
    Private processInstaller As ServiceProcessInstaller

    Public Sub New()
        MyBase.New()

        processInstaller = New ServiceProcessInstaller()
        serviceInstaller = New ServiceInstaller()

        ' Service will run under system account
        processInstaller.Account = ServiceAccount.LocalSystem

        ' Service will have Start Type of Manual
        serviceInstaller.StartType = ServiceStartMode.Manual
        serviceInstaller.ServiceName = "NTB_FtpRoute"
        'serviceInstaller.ServicesDependedOn = New String() {"NTB_XmlRouteService"}

        Installers.Add(serviceInstaller)
        Installers.Add(processInstaller)
    End Sub
End Class
